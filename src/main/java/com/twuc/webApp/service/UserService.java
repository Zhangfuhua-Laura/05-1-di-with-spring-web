package com.twuc.webApp.service;

import com.twuc.webApp.contract.GetUserResponse;
import com.twuc.webApp.domain.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class UserService {
    @Autowired
    private UserRepository repository;

    public Optional<GetUserResponse> getUser(Long id) {
        return repository.findById(id).map(GetUserResponse::new);
    }
}
