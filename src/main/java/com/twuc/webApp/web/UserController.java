package com.twuc.webApp.web;

import com.twuc.webApp.contract.GetUserResponse;
import com.twuc.webApp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping("/users")
public class UserController {
    @Autowired
    private UserService service;

    @GetMapping("/{userId}")
    ResponseEntity<GetUserResponse> getUser(@PathVariable long userId){

        Optional<GetUserResponse> getUserResponse = service.getUser(userId);
        if(getUserResponse.isPresent())
            return ResponseEntity.status(200).body(getUserResponse.get());
        else
            return ResponseEntity.status(404).body(null);
    }
}
